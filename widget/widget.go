package widget

import (
	"fmt"
	"strings"
)

// JS returns the script tag for importing in a template
func JS() string {
	return `<script src="https://hcaptcha.com/1/api.js" async defer></script>`
}

// HTML returns a div to include in a form
func HTML(siteKey string, classes ...string) string {
	return fmt.Sprintf(`<div class="h-captcha %s" data-sitekey="%s"></div>`, strings.Join(classes, " "), siteKey)
}
