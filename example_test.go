package hcaptcha

import "fmt"

func ExampleClient_Verify() {
	// Dummy secret https://docs.hcaptcha.com/#integrationtest
	client, err := New(dummySecret)
	if err != nil {
		panic(err)
	}

	// Dummy token https://docs.hcaptcha.com/#integrationtest
	resp, err := client.Verify(dummyToken, PostOptions{})
	if err != nil {
		panic(err)
	}

	if resp.Success {
		fmt.Println("Verification succeeded")
		// Output: Verification succeeded
	}
}
